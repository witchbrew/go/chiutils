package middleware

import (
	"github.com/rs/zerolog/log"
	"net/http"
)

func Zerolog(next http.Handler) http.Handler {
	f := func(writer http.ResponseWriter, request *http.Request) {
		logger := log.Logger
		ctx := logger.WithContext(request.Context())
		next.ServeHTTP(writer, request.WithContext(ctx))
	}
	return http.HandlerFunc(f)
}

func ZerologRequestID(next http.Handler) http.Handler {
	f := func(writer http.ResponseWriter, request *http.Request) {
		requestID := GetRequestID(request.Context())
		logger := log.Ctx(request.Context())
		if requestID != "" {
			l := logger.With().Str("requestID", requestID).Logger()
			logger = &l
		}
		ctxWithLogger := logger.WithContext(request.Context())
		next.ServeHTTP(writer, request.WithContext(ctxWithLogger))
	}
	return http.HandlerFunc(f)
}
