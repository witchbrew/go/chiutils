package middleware

import (
	"context"
	"github.com/google/uuid"
	"net/http"
)

type requestIDCtxKey struct{}

var RequestIDHeader = "X-Request-ID"

func RequestID(next http.Handler) http.Handler {
	f := func(writer http.ResponseWriter, request *http.Request) {
		ctx := request.Context()
		requestID := request.Header.Get(RequestIDHeader)
		if requestID == "" {
			requestID = uuid.New().String()
		}
		writer.Header().Set(RequestIDHeader, requestID)
		ctx = context.WithValue(ctx, requestIDCtxKey{}, requestID)
		next.ServeHTTP(writer, request.WithContext(ctx))
	}
	return http.HandlerFunc(f)
}

func GetRequestID(ctx context.Context) string {
	if ctx == nil {
		return ""
	}
	if reqID, ok := ctx.Value(requestIDCtxKey{}).(string); ok {
		return reqID
	}
	return ""
}
