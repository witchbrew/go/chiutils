package middleware

import (
	"github.com/go-chi/chi"
	"github.com/stretchr/testify/require"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestRequestID_NotEmpty(t *testing.T) {
	request := httptest.NewRequest("GET", "/", nil)
	writer := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Use(RequestID)
	r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusOK)
	})
	r.ServeHTTP(writer, request)
	requestID := writer.Header().Get(RequestIDHeader)
	require.NotEqual(t, "", requestID)
}

func TestRequestID_Predefined(t *testing.T) {
	request := httptest.NewRequest("GET", "/", nil)
	request.Header.Set(RequestIDHeader, "hello")
	writer := httptest.NewRecorder()
	r := chi.NewRouter()
	r.Use(RequestID)
	r.Get("/", func(writer http.ResponseWriter, request *http.Request) {
		writer.WriteHeader(http.StatusOK)
	})
	r.ServeHTTP(writer, request)
	requestID := writer.Header().Get(RequestIDHeader)
	require.Equal(t, "hello", requestID)
}